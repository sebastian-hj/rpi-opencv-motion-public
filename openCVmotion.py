import argparse
import datetime
import imutils
import json
import time
import cv2
import os
import platform
import sys
from pydrive.drive import GoogleDrive
from pydrive.auth import GoogleAuth
import logging


def setupLogFile(type, filename):
    if type == 1:
        # logging.basicConfig(filename=filename, filemode='w', level=logging.DEBUG,
        #                     format='%(asctime)s - %(levelname)s - %(message)s')
        logging.basicConfig(filename=filename, filemode='w', level=logging.INFO)
        logging.getLogger().addHandler(logging.StreamHandler())
    else:
        logging.basicConfig(level=logging.INFO)


def checkPlatform():
    # Check OS/platform
    uname = platform.uname()
    if uname[0] == 'Linux':
        runningOn = 'Linux'
        if uname[4].startswith('arm'):
            runningOn = 'RPi'
    elif uname[0] == 'Windows':
        runningOn = 'Windows'
    else:
        logging.error('Unsupported OS...')
        sys.exit(1)

    logging.debug('Platform is ' + runningOn)

    return runningOn


def importEmailClass():
    # Import email helper functions
    # Class may be in one of two ways
    # Check first option
    try:
        sys.path.insert(0, '../CheckSendEmail')
        from emailClass import MyEmail
    except ImportError:
        emailAvailable = False
    else:
        emailAvailable = True
    # Check second option
    if not emailAvailable:
        try:
            sys.path.insert(0, '../20150920_checkSendEmail')
            from emailClass import MyEmail
        except ImportError:
            emailAvailable = False
        else:
            emailAvailable = True
    if not emailAvailable:
        logging.warning(' ''emailClass'' not found.')

    return emailAvailable


def setGoogleDrive(conf):
    isUploadGoogleDrive = False
    gauth = None
    drive = None
    folderID = None
    if conf['uploadToGoogleDrive']:
        isUploadGoogleDrive = True
        # To do: validate 'settings.yaml' is present in directory
        gauth = GoogleAuth(settings_file='settings.yaml')
        # Refresh credentials (requires 'credentials.json' file at the same location)
        # Reference: http://stackoverflow.com/questions/24419188/automating-pydrive-verification-process
        if gauth.credentials is None:
            gauth.LocalWebserverAuth()
        elif gauth.access_token_expired:
            gauth.Refresh()
        else:
            gauth.Authorize()
        drive = GoogleDrive(gauth)
        # If no folder ID has been defined, use default
        if 'googleDriveFolderID' not in conf or not conf['googleDriveFolderID']:
            logging.error('No GoogleDrive folder ID to use...')
            sys.exit(1)
        else:
            folderID = conf['googleDriveFolderID']

    return isUploadGoogleDrive, gauth, drive, folderID


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Motion detection using OpenCV.')
    parser.add_argument('config', nargs='?', type=str, help='Input configuration file.')
    args = parser.parse_args()

    # Set configuration file path
    # logging.info('Reading configuration file')
    # If no argument is provided, assume same directory and 'configuration.json' name
    if args.config is None:
        confPath = 'configuration.json'
    else:
        confPath = args.config

    # Read configuration file
    try:
        conf = json.load(open(confPath))
    except IOError:
        sys.exit('Cannot find configuration file!')
    except ValueError:
        sys.exit('Configuration file not in JSON format!')

    # Check if log file is to be used
    if conf['saveLog']:
        setupLogFile(1,'test.log')
    else:
        setupLogFile(2,None)

    # Check OS/platform
    runningOn = checkPlatform()
    # Check email class
    isEmailAvailable = importEmailClass()

    # Notify of start
    logging.info('Setting up...')

    # Check if need to save images
    isSaveImages = conf['saveImages']

    # Check if need to send images
    if isEmailAvailable:
        isSendImages = conf['sendImages']
        # If send images, get white list of emails
        if isSendImages and conf['emailSendList']:
            emailSendList = conf['emailSendList']
        else:
            isSendImages = False
    else:
        isSendImages = False

    # Check if images should be uploaded to Google Drive
    isUploadGoogleDrive, gauth, drive, folderID = setGoogleDrive(conf)

    # Check if on RPi or if on computer
    if runningOn == 'RPi':
        # Import packages for camera on Raspberry Pi
        try:
            from picamera.array import PiRGBArray
            from picamera import PiCamera
        except ImportError:
            logging.error(' ''PiCamera'' not installed!')
            sys.exit(1)

        # Initialize the camera and grab a reference to the raw camera capture
        camera = PiCamera()
        camera.resolution = tuple(conf['resolution'])
        camera.framerate = conf['fps']
        rawCapture = PiRGBArray(camera, size=tuple(conf['resolution']))
    else:
        # Start video assuming webcam is device 0
        camera = cv2.VideoCapture(0)

    # Allow the camera to warm-up, then initialize the average frame, last uploaded timestamp, and
    # frame motion counter
    logging.info('Warming up...')
    time.sleep(conf['cameraWarmupTime'])
    avg = None
    lastUploaded = datetime.datetime.now()
    motionCounter = 0

    # Warm-up flag
    warmUp = True

    # Capture frames from the camera
    while True:
        # Check if on RPi or if on computer
        if runningOn == 'RPi':
            # Return type from command is unknown for now. Use work-around to get frame
            for f in camera.capture_continuous(rawCapture, format='bgr', use_video_port=True):
                frame = f.array
                break
        else:
            # Get frame from camera
            ret, frame = camera.read()

        # Set time stamp and status
        timestamp = datetime.datetime.now()
        status = 'Unoccupied'

        # Resize the frame, convert it to gray scale, and blur it
        frame = imutils.resize(frame, width=500)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        gray = cv2.GaussianBlur(gray, (21, 21), 0)

        # If the average frame is 'None', initialize it
        if avg is None:
            logging.info('Starting backgound model...')
            avg = gray.copy().astype('float')
            if runningOn == 'RPi':
                rawCapture.truncate(0)
            continue

        if warmUp is True:
            warmUp = False
            logging.info('Done warming up.')

        # Accumulate the weighted average between the current frame and previous frames, then
        # compute the difference between the current frame and running average
        cv2.accumulateWeighted(gray, avg, 0.5)
        frameDelta = cv2.absdiff(gray, cv2.convertScaleAbs(avg))

        # Threshold the delta image, dilate the threshold'ed image to fill in holes, then find
        # contours on threshold'ed image
        thresh = cv2.threshold(frameDelta, conf['deltaThresh'], 255, cv2.THRESH_BINARY)[1]
        thresh = cv2.dilate(thresh, None, iterations=2)
        # To avoid:
        # 'ValueError: too many values to unpack'
        # 'cv2.findContours' returns three values
        # Reference:
        # http://stackoverflow.com/questions/27746089/python-computer-vision-contours-too-many-
        # values-to-unpack
        (_, cnts, _) = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        # Loop over the contours
        for c in cnts:
            # If the contour is too small, ignore it
            if cv2.contourArea(c) < conf['minArea']:
                continue

            # Compute the bounding box for the contour, draw it on the frame,and update 'status'
            (x, y, w, h) = cv2.boundingRect(c)
            cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
            status = 'Occupied'

        # Draw the text and timestamp on the frame
        ts = timestamp.strftime('%A %d %B %Y %H:%M:%S')
        cv2.putText(frame, 'Room Status: {}'.format(status), (10, 20), cv2.FONT_HERSHEY_SIMPLEX,
                    0.5, (0, 0, 255), 2)
        cv2.putText(frame, ts, (10, frame.shape[0] - 10), cv2.FONT_HERSHEY_SIMPLEX, 0.35,
                    (0, 0, 255), 1)

        # Check to see if the room is occupied
        if status == 'Occupied':
            # Check to see if enough time has passed between uploads
            if (timestamp - lastUploaded).seconds >= conf['minUploadSeconds']:
                # Increment the motion counter
                motionCounter += 1
                # Check to see if the number of frames with consistent motion is high enough
                if motionCounter >= conf['minMotionFrames']:
                    # Show message in console about motion
                    timeStampConsole = timestamp.strftime('%Y%m%d%A_%H%M%S')
                    logging.info('Motion at ' + timeStampConsole)

                    if isSaveImages or isSendImages:
                        # Save image
                        absolutePath = os.getcwd() + '/'
                        outFileName = absolutePath + timeStampConsole + '.png'
                        cv2.imwrite(outFileName, frame)
                        # Check if there is need to send image
                        if isSendImages:
                            logging.warning('Email needs to be set-up in JSON. Support to be ' \
                                  'added soon')
                            # # Start email class
                            # # email =
                            # # password =
                            # # pbAPIkey =
                            # emailTest = MyEmail(email, password, None, True)
                            # # Set email subject and body
                            # emailSubject = "Raspberry Pi"
                            # emailBody = "Photo taken at " + timeStampConsole
                            # # Send email
                            # emailTest.sendToEmail(emailSendList, emailSubject, emailBody,
                            #                       [outFileName])
                        # Check if image should be uploaded to Google Drive
                        if isUploadGoogleDrive:
                            # Refresh token if expired
                            if gauth.access_token_expired:
                                gauth.Refresh()
                            outFileNameSimple = timeStampConsole + '.png'
                            try:
                                f = drive.CreateFile({'parents': [{'kind': 'drive#fileLink',
                                                                   'id': folderID}]})
                                f.SetContentFile(outFileNameSimple)
                                f.Upload()
                            except Exception as e:
                                logging.debug('Could not upload ' + outFileNameSimple 
                                              + ' to Google Drive..')
                                logging.debug('Error is')
                                logging.debug(e.__class__, e.__doc__, e.message)
                        # If image should not be saved, delete it
                        if not isSaveImages:
                            os.remove(outFileName)

                    # Update the last uploaded timestamp and reset the motion counter
                    lastUploaded = timestamp
                    motionCounter = 0

        # Otherwise, the room is not occupied
        else:
            motionCounter = 0

        # Check to see if the frames should be displayed to screen
        if conf['showVideo']:
            # Display video
            cv2.imshow('Stream', frame)
            key = cv2.waitKey(1) & 0xFF

            # If the 'q' key is pressed, break from the loop
            if key == ord('q'):
                break

        # Clear the stream in preparation for the next frame
        if runningOn == 'RPi':
            rawCapture.truncate(0)
