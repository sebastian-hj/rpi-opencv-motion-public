## Introduction

### Reference

[Home surveillance and motion detection with the Raspberry Pi, Python, OpenCV, and Dropbox](http://www.pyimagesearch.com/2015/06/01/home-surveillance-and-motion-detection-with-the-raspberry-pi-python-and-opencv/) by Adrian Rosebrock.

### Description

Modified reference online example to detect motion using live camera video stream. Notify of movement
via UI, email, push notifications, or Google Drive.

## Installation

### Requirements
- OpenCV (tested on version 3.1.0)
- Python 2.7 (tested versions 2.7.11 and 2.7.12)
- The following Python packages:
    - _[imutils](http://www.pyimagesearch.com/2015/02/02/just-open-sourced-personal-imutils-package-series-opencv-convenience-functions/)_
    - _[pydrive](http://pythonhosted.org/PyDrive/)_
    - _emailClass_ (currently in private repository)

### Process

To be explained...


## Things to do

- [ ] Set-up email through JSON.
- [ ] Make _emailClass_ public.
